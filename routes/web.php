<?php

/** @var \Laravel\Lumen\Routing\Router $router */
$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->group(['prefix' => 'api/v1'], function () use ($router) {
    $router->post('login', 'LoginController@postLogin');
	$router->post('examples', 'ExampleController@store');
	$router->put('examples', 'ExampleController@update');
	$router->get('examples', 'ExampleController@index');
	$router->get('examples/{exampleId}', 'ExampleController@show');
	$router->delete('examples/{exampleId}', 'ExampleController@delete');
});
