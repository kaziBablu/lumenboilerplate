<?php

namespace App\Http\Controllers;

use App\Http\Resources\API\GenericResponse;
use App\Http\Resources\API\V1\Example;
use App\Services\Example\ExampleService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class ExampleController extends Controller
{
    /**
     * @var
     */
    private $exampleService;

    /**
     * ExampleController constructor.
     * @param ExampleService $exampleService
     */
    public function __construct(ExampleService $exampleService)
    {
        $this->exampleService = $exampleService;
    }

    /**
     * @param Request $request
     * @return GenericResponse
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required'
            ]);
        } catch (ValidationException $e) {
            $payload = [
                'code' => Response::HTTP_FORBIDDEN,
                'status' => false,
                'app_message' => 'Validation Fail',
                'user_message' => $e->errors(),
            ];

            return new GenericResponse('validation-fail', null, $payload, Response::HTTP_FORBIDDEN);
        }

        $example = $this->exampleServic->store($request);

        $payload = [
            'code' => Response::HTTP_OK,
            'status' => true,
            'app_message' => 'Example Created',
            'user_message' => 'Example Created.',
        ];

        return new GenericResponse('Example Created', null, $payload, Response::HTTP_OK);
    }

    /**
     * @return ErrorResponse|GenericResponse
     */
    public function index()
    {
        $example = $this->exampleService->all();

        if (count($exampleService) > 0) {
            $payload = (new ResponsePayload("Data found."))->toArray();

            return new GenericResponse('example-list', new Example($example), $payload);
        }

        $payload = (new ResponsePayload("No data found.", FALSE, NULL, Response::HTTP_NOT_FOUND))->toArray();

        return new ErrorResponse('example-list', $payload, Response::HTTP_NOT_FOUND);
    }

    /**
     * @param $data
     * @return ErrorResponse|GenericResponse
     */
    public function show($data)
    {
        $example = $this->exampleService->show($data);
		
        if (count($showExample) > 0) {
            $payload = (new ResponsePayload("Data found."))->toArray();

            return new GenericResponse('example-list', new Example($example), $payload);
        }

        $payload = (new ResponsePayload("No data found.", FALSE, NULL, Response::HTTP_NOT_FOUND))->toArray();

        return new ErrorResponse('example-list', $payload, Response::HTTP_NOT_FOUND);
    }
    /**
     * @param Request $request
     * @param $data
     * @return GenericResponse
     */
    public function update(Request $request, $data)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required'
            ]);
        } catch (ValidationException $e) {
            $payload = [
                'code' => Response::HTTP_FORBIDDEN,
                'status' => false,
                'app_message' => 'Validation Fail',
                'user_message' => $e->errors(),
            ];

            return new GenericResponse('validation-fail', null, $payload, Response::HTTP_FORBIDDEN);
        }

        $example = $this->exampleServic->update($data);
		
        $payload = [
            'code' => Response::HTTP_OK,
            'status' => true,
            'app_message' => 'Example Update',
            'user_message' => 'Example Update.',
        ];
        return new GenericResponse('example-update', new Example($example), $payload, Response::HTTP_OK);

    }

    public function delete($data)
    {
        $this->exampleServic->delete($data);
		
        $payload = [
            'code' => Response::HTTP_OK,
			'status' => true,
            'app_message' => 'Example Delete',
            'user_message' => 'Example Delete.',
        ];

        return new GenericResponse('example-delete', null, $payload, Response::HTTP_OK);
    }
}
