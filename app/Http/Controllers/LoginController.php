<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;
use Shuttle\PhpPack\Traits\ShuttlePhpPackTrait;

class LoginController extends Controller
{
    use ShuttlePhpPackTrait;

    public function postLogin(Request $request)
    {
        $url = 'auth';
        if (is_numeric(trim($request->primary_id))) {
            $token = $this->setMobileAndPasswordStrategy($request, $url);
            return $token;
        }
    }

    /**
     * @param $request
     * @param string $url
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function setMobileAndPasswordStrategy($request, string $url)
    {
        $requestArray = array(
            'mobile' => $request->primary_id,
            'secret' => $request->secret,
            'strategy' => 'MobileAndPassword '
        );
        $userService = new UserService();
        return $userService->getToken($requestArray, $url);
    }
}
