<?php


namespace App\Exceptions;

use Exception;
use Throwable;
use Illuminate\Http\Response;

class ValidationFailException extends Exception
{
    /**
     * ValidationFailException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = Response::HTTP_FORBIDDEN, Throwable $previous = null)
    {
        parent::__construct($message, Response::HTTP_FORBIDDEN, $previous);
        $this->code = $code ?: Response::HTTP_FORBIDDEN;
    }

    /**
     * @return mixed
     */
    public function response()
    {
        return $this->response;
    }

    /**
     * @param $response
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * @return mixed
     */
    public function toResponse()
    {
        return Response::deny($this->message, $this->code);
    }
}
