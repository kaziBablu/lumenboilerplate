<?php


namespace App\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class UserService
{
    public function getToken(array $requestArray, $url)
    {
        $http = new Client;
        $userServiceUrl = env('USER_SERVICE_URL');
        $response = $http->post($userServiceUrl . '/' . $url, [
            'http_errors' => false,
            'form_params' => [
                array_keys($requestArray)[0] => $requestArray['mobile'] ? $requestArray['mobile'] : $requestArray['email'],
                array_keys($requestArray)[1] => $requestArray['secret'],
                array_keys($requestArray)[2] => $requestArray['strategy']
            ],
        ]);
        return $data = json_decode((string)$response->getBody(), true);
    }
}
