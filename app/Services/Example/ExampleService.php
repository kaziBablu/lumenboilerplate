<?php


namespace App\Services\Example;


interface ExampleService
{
    /**
     * @param $data
     * @return mixed
     */
    public function store($data);

    /**
     * @return mixed\
     */
    public function all();

    /**
     * @return mixed
     */
    public function show($data);

    /**
     * @param $data
     * @return mixed
     */
    public function edit($data);

    /**
     * @param $data
     * @return mixed
     */
    public function update($data);

    /**
     * @param $data
     * @return mixed
     */
    public function delete($data);
}
