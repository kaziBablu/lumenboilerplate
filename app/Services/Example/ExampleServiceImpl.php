<?php

namespace App\Services\Example;

use App\Repositories\Example\ExampleRepository;


class ExampleServiceImpl implements ExampleService
{
	
	private $exampleRepository;

    public function __construct
	(
		ExampleRepository	$exampleRepository
	)
	{
        $this->exampleRepository	= $exampleRepository;
    }
	
    /**
     * @param $data
     * @return mixed|void
     */
    public function store($data)
    {
        // TODO: write business logic(if has) and Implement store() method.
		return $this->exampleRepository->store();
    }

    /**
     * @return mixed\|void
     */
    public function all()
    {
        // TODO: write business logic(if has) and Implement all() method.
		return $this->exampleRepository->all();
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function show($data)
    {
        // TODO: write business logic(if has) and Implement show() method.
		return $this->exampleRepository->show();
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function edit($data)
    {
        // TODO: write business logic(if has) and Implement edit() method.
		return $this->exampleRepository->edit();
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function update($data)
    {
        // TODO: write business logic(if has) and Implement update() method.
		return $this->exampleRepository->update();
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function delete($data)
    {
        // TODO: write business logic(if has) and Implement delete() method.
		return $this->exampleRepository->delete();
    }
}
