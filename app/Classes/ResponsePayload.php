<?php

namespace App\Classes;

use Illuminate\Http\Response;

class ResponsePayload
{
    private $app_message;
    private $status;
    private $user_messege;
    private $code;

    /**
     * ResponsePayload constructor.
     * @param $app_message
     * @param bool $status
     * @param null $user_messege
     * @param int $code
     */
    function __construct($app_message, $status = TRUE, $user_messege = NULL, $code = Response::HTTP_OK)
    {
        $this->app_message = $app_message;
        $this->user_messege = $user_messege ?? $app_message; // optional
        $this->status = $status;
        $this->code = $code;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'status' => $this->status,
            'code' => $this->code,
            'app_message' => $this->app_message,
            'user_messege' => $this->user_messege
        ];
    }
}
