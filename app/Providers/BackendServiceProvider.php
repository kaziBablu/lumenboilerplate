<?php

namespace App\Providers;

use App\Repositories\Example\ExampleInterface;
use App\Repositories\Example\ExampleRepository;
use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Example\ExampleRepository',
            'App\Repositories\Example\ExampleRepositoryImpl',
        );
		
		$this->app->bind(
            'App\Services\Example\ExampleService',
            'App\Services\Example\ExampleServiceImpl',
        );
    }
}
