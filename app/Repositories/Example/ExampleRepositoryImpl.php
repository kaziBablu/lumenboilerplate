<?php


namespace App\Repositories\Example;


class ExampleRepositoryImpl implements ExampleRepository
{
    /**
     * @param $data
     * @return mixed|void
     */
    public function store($data)
    {
        // TODO: Implement store() method.
    }

    /**
     * @return mixed\|void
     */
    public function all()
    {
        // TODO: Implement getData() method.
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function show($data)
    {
        // TODO: Implement show() method.
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function edit($data)
    {
        // TODO: Implement edit() method.
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function update($data)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function delete($data)
    {
        // TODO: Implement delete() method.
    }
}
